import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    aceleracion: 10,
    velocidad: 0,
    desaceleracion: 10,
  },
  mutations: {
    acelerar(state) {
      state.velocidad += state.aceleracion;
    },
    frenar(state) {
      state.velocidad -= state.desaceleracion;
    }
  }
});
